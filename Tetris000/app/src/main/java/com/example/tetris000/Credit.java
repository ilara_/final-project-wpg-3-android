package com.example.tetris000;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Credit extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit);
    }

    public void onExit(View v){
        Intent intent = new Intent(this, StartScreen.class);
        startActivity(intent);
        finish();
    }
}
