package com.example.tetris000;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class StartScreen extends AppCompatActivity {

    Marathon parent;
    SharedPreferences mPref;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_screen);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        readPreferences();
        mPref = getSharedPreferences("setup", MODE_PRIVATE);
    }

    public void readPreferences(){
        try {
            parent.randomType = mPref.getInt("Random Type", 1); // Second one means get what value if this does not exist
        } catch (Exception PrefEmpty) {
            parent.randomType = 1;
        }
        try {
            parent.rotationType = mPref.getInt("Rotation Type", 1); // Second one means get what value if this does not exist
        } catch (Exception PrefEmpty) {
            parent.rotationType = 2;
        }
        try {
            parent.softDropSpeed = mPref.getInt("Soft Drop Type", 1); // Second one means get what value if this does not exist
        } catch (Exception PrefEmpty) {
            parent.softDropSpeed = 1;
        }
    }

    public void onStart(View v){
        parent.playMode = "Marathon";
        Intent intent = new Intent(this, Marathon.class);
        startActivity(intent);
        finish();
    }

    public void onCredit(View v){
        Intent intent = new Intent(this, Credit.class);
        startActivity(intent);
        finish();
    }

    public void onExit(View v){
        finish();
    }
}
